<?php

namespace Tigris\InvoiceBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Repository\UserRepository;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\InvoiceBundle\Entity\Invoice;
use Tigris\InvoiceBundle\Form\Type\InvoiceType;
use Tigris\InvoiceBundle\Manager\InvoiceManager;
use Tigris\InvoiceBundle\Repository\InvoiceRepository;

#[Route('/admin/invoice')]
#[IsGranted('ROLE_ADMIN')]
class InvoiceController extends BaseController
{
    use FormTrait;

    #[Route('/index')]
    public function index(UserRepository $userRepository): Response
    {
        $this->generateBreadcrumbs([
            'menu.invoices' => ['route' => 'tigris_invoice_admin_invoice_index'],
        ]);

        $user = $userRepository->findBy(['enabled' => true]);

        return $this->render("@TigrisInvoice\admin\invoice\index.html.twig", [
            'users' => $user,
        ]);
    }

    #[Route('/data', options: ['expose' => 'admin'])]
    public function data(Request $request, InvoiceRepository $invoiceRepository): Response
    {
        $criteria = $this->getCriteria($request);
        $criteria['status'] = $request->query->get('status', '');
        $criteria['payment_status'] = $request->query->get('paymentStatus', '');
        $criteria['shipping_status'] = $request->query->get('shippingStatus', '');
        $criteria['user'] = $request->query->get('user', '');

        $data = $invoiceRepository->findData($criteria);

        return $this->paginatorToJsonResponse($data);
    }

    #[Route('/new', options: ['expose' => 'admin'], methods: ['GET', 'POST'])]
    public function new(
        Request $request, 
        TranslatorInterface $translator, 
        InvoiceManager $invoiceManager, 
        EntityManagerInterface $em
    ): Response
    {
        $this->generateBreadcrumbs([
            'menu.invoices' => ['route' => 'tigris_invoice_admin_invoice_index'],
            'invoice.add.add' => ['route' => 'tigris_invoice_admin_invoice_new'],
        ]);

        $entity = $invoiceManager->create();

        $form = $this->createForm(InvoiceType::class, $entity, [
            'method' => Request::METHOD_POST,
            'action' => $this->generateUrl('tigris_invoice_admin_invoice_new'),
        ]);
        $this->addFormActions($form, $this->generateUrl('tigris_invoice_admin_invoice_index'));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->addFlash(
                'success',
                $translator->trans('invoice.edit.success')
            );

            return $this->redirectToRoute('tigris_invoice_admin_invoice_index');
        }

        return $this->render('@TigrisInvoice\admin\invoice\new.html.twig', ['form' => $form, 'entity' => $entity]);
    }

    #[Route('/{id}/edit', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['GET', 'PUT'])]
    public function edit(
        Request $request, 
        Invoice $entity, 
        TranslatorInterface $translator, 
        EntityManagerInterface $em
    ): Response
    {
        $form = $this->createForm(InvoiceType::class, $entity, [
            'method' => Request::METHOD_PUT,
            'action' => $this->generateUrl('tigris_invoice_admin_invoice_edit', ['id' => $entity->getId()]),
        ]);
        $this->addFormActions($form, $this->generateUrl('tigris_invoice_admin_invoice_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->addFlash(
                'success',
                $translator->trans('invoice.edit.success')
            );

            return $this->redirectToRoute('tigris_invoice_admin_invoice_index');
        }

        return $this->render('@TigrisInvoice\admin\invoice\edit.html.twig', ['form' => $form, 'entity' => $entity]);
    }

    #[Route('/{id}/remove', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['GET', 'DELETE'])]
    public function delete(
        Request $request, 
        Invoice $entity, 
        TranslatorInterface $translator, 
        EntityManagerInterface $em
    ): Response
    {
        $form = $this->createFormBuilder($entity, [
            'method' => Request::METHOD_DELETE,
            'action' => $this->generateUrl('tigris_invoice_admin_invoice_remove', ['id' => $entity->getId()]),
        ])->getForm();
        $this->addFormActions($form, $this->generateUrl('tigris_invoice_admin_invoice_index'), 'button.delete');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($entity);
            $em->flush();

            $this->addFlash(
                'success',
                $translator->trans('invoice.delete.success')
            );

            return $this->redirectToRoute('tigris_invoice_admin_invoice_index');
        }

        return $this->render('@TigrisInvoice\admin\invoice\remove.html.twig', ['form' => $form, 'entity' => $entity]);
    }
}
