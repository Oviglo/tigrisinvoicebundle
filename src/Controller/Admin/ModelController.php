<?php

namespace Tigris\InvoiceBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\InvoiceBundle\Entity\Model;
use Tigris\InvoiceBundle\Form\Type\ModelType;
use Tigris\InvoiceBundle\Repository\ModelRepository;

#[Route(path: '/admin/model', name: 'tigris_invoice_admin_model_')]
#[IsGranted('ROLE_ADMIN')]
class ModelController extends BaseController
{
    use FormTrait;

    #[Route(path: '/index', name: 'index')]
    public function index(): Response
    {
        $this->generateBreadcrumbs([
            'menu.invoices' => ['route' => 'tigris_invoice_admin_invoice_index'],
            'menu.invoice_models' => ['route' => 'tigris_invoice_admin_model_index'],
        ]);

        return $this->render('@TigrisInvoice\admin\model\index.html.twig');
    }

    #[Route(path: '/list.{_format}', name: 'list', options: ['expose' => 'admin'])]
    public function list(Request $request, ModelRepository $modelRepository): Response
    {
        $criteria = $this->getCriteria($request);
        $criteria['statut'] = $request->query->get('statut', 1);

        $data = $modelRepository->findData($criteria);

        return $this->paginatorToJsonResponse($data);
    }

    #[Route(path: '/post', name: 'post', options: ['expose' => 'admin'], defaults: ['_format' => 'html'], methods: ['POST'])]
    #[Route(path: '/new', name: 'new', options: ['expose' => 'admin'], defaults: ['_format' => 'html'], methods: ['GET'])]
    public function new(Request $request, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        $this->generateBreadcrumbs([
            'menu.invoices' => ['route' => 'tigris_invoice_admin_invoice_index'],
            'menu.invoice_models' => ['route' => 'tigris_invoice_admin_model_index'],
            'invoice_model.add.add' => ['route' => 'tigris_invoice_admin_model_new'],
        ]);

        $entity = new Model();

        $form = $this->createForm(ModelType::class, $entity, [
            'action' => $this->generateUrl('tigris_invoice_admin_model_new'),
            'method' => 'POST',
        ]);

        $this->addFormActions($form, 'tigris_invoice_admin_model_index');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->addFlash(
                'success',
                $translator->trans('invoice.edit.success')
            );

            return $this->redirectToRoute('tigris_invoice_admin_invoice_index');
        }

        return $this->render('@TigrisInvoice\admin\model\new.html.twig', ['form' => $form, 'entity' => $entity]);
    }

    #[Route(path: '/put/{id}', name: 'put', options: ['expose' => 'admin'], defaults: ['_format' => 'html'], methods: ['PUT'])]
    #[Route(path: '/edit/{id}', name: 'edit', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], defaults: ['_format' => 'html'], methods: ['GET'])]
    public function edit(
        Request $request,
        Model $entity,
        TranslatorInterface $translator,
        EntityManagerInterface $em
    ): Response {
        $this->generateBreadcrumbs([
            'menu.invoices' => ['route' => 'tigris_invoice_admin_invoice_index'],
            'menu.invoice_models' => ['route' => 'tigris_invoice_admin_model_index'],
            'invoice_model.edit.edit' => ['route' => 'tigris_invoice_admin_model_edit', 'params' => ['id' => $entity->getId()]],
        ]);

        $form = $this->createForm(ModelType::class, $entity, [
            'action' => $this->generateUrl('tigris_invoice_admin_model_edit', ['id' => $entity->getId()]),
            'method' => 'POST',
        ]);

        $this->addFormActions($form, 'tigris_invoice_admin_model_index');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->addFlash(
                'success',
                $translator->trans('invoice_model.edit.success')
            );

            return $this->redirectToRoute('tigris_invoice_admin_model_index');
        }

        return $this->render('@TigrisInvoice\admin\model\edit.html.twig', ['form' => $form, 'entity' => $entity]);
    }

    #[Route(path: '/delete/{id}', name: 'delete', options: ['expose' => 'admin'], defaults: ['_format' => 'json'], methods: ['DELETE'])]
    #[Route(path: '/remove/{id}', name: 'remove', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], defaults: ['_format' => 'html'], methods: ['GET'])]
    public function delete(Request $request, Model $entity, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        $this->generateBreadcrumbs([
            'menu.invoices' => ['route' => 'tigris_invoice_admin_invoice_index'],
            'menu.invoice_models' => ['route' => 'tigris_invoice_admin_model_index'],
            'invoice_model.delete.delete' => [
                'route' => 'tigris_invoice_admin_model_delete',
                'params' => ['id' => $entity->getId()],
            ],
        ]);

        $form = $this->getDeleteForm($entity, 'tigris_invoice_admin_invoice_');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($entity);
            $em->flush();

            $this->addFlash(
                'success',
                $translator->trans('invoice_model.delete.success')
            );

            return $this->redirectToRoute('tigris_invoice_admin_model_index');
        }

        return $this->render(
            '@TigrisInvoice\admin\model\remove.html.twig',
            ['form' => $form->createView(), 'entity' => $entity]
        );
    }
}
