<?php

namespace Tigris\InvoiceBundle\Controller;

use Dompdf\Dompdf;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\InvoiceBundle\Entity\Invoice;

#[Route('/invoice')]
#[IsGranted('ROLE_USER')]
class InvoiceController extends BaseController
{
    #[Route(
        path: '/{id}/show.{_format}', 
        requirements: ['id' => '\d+', '_format' => 'html|pdf'], 
        defaults: ['_format' => 'html'], 
        options: ['expose' => true]
    )]
    public function show(Invoice $entity, string $_format): Response
    {
        if ('pdf' === $_format) {
            $dompdf = new Dompdf();
            $dompdf->loadHtml($this->renderView('@TigrisInvoice/invoice/show.pdf.twig', [
                'entity' => $entity,
            ]));
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            $dompdf->stream($entity->getName().'.pdf');

            return new Response('');
        }

        return $this->render('@TigrisInvoice/invoice/show.html.twig', [
            'entity' => $entity,
        ]);
    }
}
