<?php

namespace Tigris\InvoiceBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\BaseBundle\Entity\Model\User;

#[ORM\Entity]
#[ORM\Table(name: 'invoice_customer')]
class Customer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\Column]
    private string $fullname;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private string|null $shippingAddress = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private string|null $billingAddress = null;

    #[ORM\Column(nullable: true)]
    #[Assert\Email(mode: Assert\Email::VALIDATION_MODE_HTML5)]
    private ?string $email = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private User|null $user = null;

    public function getId(): int|null
    {
        return $this->id;
    }

    public function getFullname(): string
    {
        return $this->fullname;
    }

    public function setFullname(string $fullname): self
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function getShippingAddress(): string
    {
        return $this->shippingAddress ?? '';
    }

    public function setShippingAddress(string $shippingAddress = null): self
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    public function getBillingAddress(): string
    {
        return $this->billingAddress ?? '';
    }

    public function setBillingAddress(string $billingAddress): self
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUser(): User|null
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
