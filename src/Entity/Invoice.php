<?php

namespace Tigris\InvoiceBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\InvoiceBundle\Invoice\Calculator;
use Tigris\InvoiceBundle\Invoice\InvoiceItem;
use Tigris\InvoiceBundle\Repository\InvoiceRepository;

#[ORM\Entity(repositoryClass: InvoiceRepository::class)]
#[ORM\Table(name: 'invoice')]
class Invoice
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    final public const STATUS_NEW = 'new';
    final public const STATUS_FINISHED = 'finished';
    final public const STATUS_CANCELED = 'canceled';
    final public const STATUS_AWAITING_PAYMENT = 'awaiting_payment';
    final public const STATUS_PAID = 'paid';
    final public const STATUS_READY = 'ready';
    final public const STATUS_SHIPPED = 'shipped';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\Column(length: 180)]
    #[Assert\NotBlank]
    private string $name;

    #[ORM\Column(length: 30)]
    private string $status = self::STATUS_NEW;

    #[ORM\Column(length: 30)]
    private string $paymentStatus = self::STATUS_AWAITING_PAYMENT;

    #[ORM\Column(length: 30)]
    private string $shippingStatus = self::STATUS_READY;

    #[ORM\Column]
    #[Assert\NotBlank]
    private int $number = 0;

    #[ORM\Column(nullable: true)]
    private string|null $numberPrefix = null;

    #[ORM\Column(type: Types::JSON)]
    private array $items = [];

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private string|null $vendorAddress = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private string|null $shippingAddress = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private string|null $billingAddress = null;

    #[ORM\ManyToOne(cascade: ['all'])]
    private Customer $customer;

    #[JMS\AccessType(type: 'public_method')]
    private float $amountET = 0.0;

    #[JMS\AccessType(type: 'public_method')]
    private float $amountIT = 0.0;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private string|null $message = null;

    #[ORM\Column(nullable: true)]
    private float|null $discount = null;

    #[ORM\Column(nullable: true)]
    private string|null $discountType = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $date;

    #[ORM\ManyToOne]
    private Model|null $model = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private User|null $author = null;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    public function getId(): int|null
    {
        return $this->id;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getVendorAddress(): string
    {
        return $this->vendorAddress ?? '';
    }

    public function setVendorAddress(string $vendorAddress): self
    {
        $this->vendorAddress = $vendorAddress;

        return $this;
    }

    public function getShippingAddress(): string
    {
        return $this->shippingAddress ?? '';
    }

    public function setShippingAddress(string $shippingAddress): self
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    public function getBillingAddress(): string
    {
        return $this->billingAddress ?? '';
    }

    public function setBillingAddress(string $billingAddress): self
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    public function getCustomer(): Customer|null
    {
        return $this->customer;
    }

    public function setCustomer(Customer $customer = null): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getAmountET(): float
    {
        $total = 0;

        foreach ($this->items as $item) {
            $amount = $item['amountET'];
            $total += ($amount * (int) $item['quantity']);
        }

        return $total;
    }

    public function getTaxAmount()
    {
        $taxes = [];
        foreach ($this->items as $item) {
            foreach ($item['taxes'] as $name => $value) {
                $taxId = $name . ' ' . $value . '%';
                if (!isset($taxes[$taxId])) {
                    $taxes[$taxId] = 0;
                }
                // PRIXTTC*100/(100+TVA)
                $taxes[$taxId] += Calculator::amountIT($item['amountET'], $value);
            }
        }

        return $taxes;
    }

    public function setAmountET(float $amountET): self
    {
        $this->amountET = $amountET;

        return $this;
    }

    public function setAmountIT(float $value): self
    {
        return $this;
    }

    public function getAmountIT(): float
    {
        $total = 0;

        foreach ($this->items as $item) {
            $amount = $item['amountIT'] ?? $item['amountET'];
            $total += ($amount * (int) $item['quantity']);
        }

        return $total;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function setItems(array $items): self
    {
        if (is_string($items)) {
            $items = json_decode($items, null, 512, JSON_THROW_ON_ERROR);
        }
        $this->items = $items;

        return $this;
    }

    public function addItem($item): self
    {
        if ($item instanceof InvoiceItem) {
            $item = $item->toArray();
        } elseif (is_string($item)) {
            $item = json_decode($item, null, 512, JSON_THROW_ON_ERROR);
        }

        $this->items[] = $item;

        return $this;
    }

    public function getTotal(): float
    {
        return Calculator::totalIT($this->items);
    }

    public function getMessage(): string
    {
        return $this->message ?? '';
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getDiscount(): float
    {
        return $this->discount ?? 0.0;
    }

    public function setDiscount(float $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getDiscountType(): string|null
    {
        return $this->discountType;
    }

    public function setDiscountType(string|null $discountType): self
    {
        $this->discountType = $discountType;

        return $this;
    }

    public function getDate(): \DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getModel(): Model|null
    {
        return $this->model;
    }

    public function setModel(Model $model = null): self
    {
        $this->model = $model;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status ?? static::STATUS_NEW;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPaymentStatus(): string
    {
        return $this->paymentStatus ?? static::STATUS_AWAITING_PAYMENT;
    }

    public function setPaymentStatus(string $paymentStatus): self
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    public function getShippingStatus(): string
    {
        return $this->shippingStatus ?? static::STATUS_READY;
    }

    public function setShippingStatus(string $shippingStatus): self
    {
        $this->shippingStatus = $shippingStatus;

        return $this;
    }

    #[JMS\VirtualProperty]
    public function getNumberStr(): string
    {
        return $this->numberPrefix . str_pad($this->number, 6, '0', STR_PAD_LEFT);
    }

    public function getAuthor(): User|null
    {
        return $this->author;
    }

    public function setAuthor(User $author = null): self
    {
        $this->author = $author;

        return $this;
    }

    public function getNumberPrefix(): string
    {
        return $this->numberPrefix ?? '';
    }

    public function setNumberPrefix(string $numberPrefix = null): self
    {
        $this->numberPrefix = $numberPrefix;

        return $this;
    }

    #[JMS\VirtualProperty()]
    public function isLocked(): bool
    {
        return self::STATUS_FINISHED === $this->status;
    }
}
