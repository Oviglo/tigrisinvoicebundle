<?php

namespace Tigris\InvoiceBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\FormEvent;
use Tigris\InvoiceBundle\Form\Type\ConfigType;

class ConfigListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            Events::LOAD_CONFIGS => 'onLoadConfigs',
        ];
    }

    public function onLoadConfigs(FormEvent $event): void
    {
        $form = $event->getForm();
        $form->add('TigrisInvoiceBundle', ConfigType::class, [
            'label' => 'config.invoice',
        ]);

        $event->setForm($form);
    }
}
