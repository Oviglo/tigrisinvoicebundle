<?php

namespace Tigris\InvoiceBundle\EventListener;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\ORM\Events;
use Tigris\BaseBundle\Manager\ConfigManager;
use Tigris\InvoiceBundle\Entity\Invoice;

#[AsDoctrineListener(Events::postPersist)]
class DoctrineListener
{
    public function __construct(private readonly ConfigManager $configManager)
    {
    }

    /**
     * Update next invoice number.
     */
    public function postPersist(PostPersistEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof Invoice) {
            return;
        }

        $this->configManager->setConfig('TigrisInvoiceBundle', 'next_invoice_number', $entity->getNumber() + 1);
    }
}
