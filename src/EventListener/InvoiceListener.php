<?php

namespace Tigris\InvoiceBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Tigris\InvoiceBundle\Entity\Invoice;

class InvoiceListener implements EventSubscriberInterface
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
        ];
    }

    public function createInvoice(GenericEvent $event): void
    {
        $entity = (new Invoice())
            ->setName($event->getArgument('name'))
            ->setBillingAddress($event->getArgument('billing_address'))
            ->setShippingAddress($event->getArgument('shipping_address'))
        ;

        foreach ($event->getArguments() as $item) {
            $entity->addItem($item);
        }

        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }
}
