<?php

namespace Tigris\InvoiceBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\MenuEvent;

class MenuListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            Events::LOAD_ADMIN_MENU => 'onLoadAdminMenu',
        ];
    }

    public function onLoadAdminMenu(MenuEvent $event): void
    {
        $menu = $event->getMenu();

        $parent = $menu->addChild('menu.billing', [
            'uri' => '#',
        ]);

        $parent->addChild('menu.invoices', [
            'route' => 'tigris_invoice_admin_invoice_index',
        ]);

        /* $parent->addChild('menu.invoice_models', array(
            'route' => 'tigris_invoice_admin_model_index',
        )); */
    }
}
