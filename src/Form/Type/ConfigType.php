<?php

namespace Tigris\InvoiceBundle\Form\Type;

use App\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\BaseBundle\Form\Type\WysiwygType;

class ConfigType extends AbstractType
{
    public function __construct(private readonly Security $security)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $user = $this->security->getUser();

        $builder
            ->add('default_address', WysiwygType::class, [
                'label' => 'invoice.config.default_address',
                'required' => false,
                'tools' => 'mini',
            ])

            ->add('next_invoice_number', NumberType::class, [
                'label' => 'invoice.config.next_invoice_number',
                'required' => false,
                'constraints' => [
                    new Assert\Range([
                        'min' => 1,
                        'max' => 9_999_999_999,
                    ]),
                ],
            ])

            ->add('invoice_prefix', TextType::class, [
                'label' => 'invoice.config.invoice_prefix',
                'required' => false,
            ])
        ;

        if ($user instanceof User && $user->isSuperAdmin()) {
            $builder->get('next_invoice_number')
                ->addModelTransformer(new CallbackTransformer(
                    fn ($valueAsFloat) => (int) $valueAsFloat,
                    fn ($valueAsString) => $valueAsString
                ))
            ;
        }
    }
}
