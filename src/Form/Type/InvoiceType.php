<?php

namespace Tigris\InvoiceBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Form\Type\StaticType;
use Tigris\BaseBundle\Form\Type\WysiwygType;
use Tigris\InvoiceBundle\Entity\Invoice;

class InvoiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => 'invoice.name',
            ])

            ->add('billingAddress', WysiwygType::class, [
                'label' => 'invoice.billing_address',
                'required' => false,
            ])
            ->add('vendorAddress', WysiwygType::class, [
                'label' => 'invoice.vendor_address',
                'required' => false,
            ])

            ->add('message', null, [
                'label' => 'invoice.message',
                'attr' => [
                    'rows' => 4,
                ],
            ])

            ->add('discount', NumberType::class, [
                'label' => 'invoice.discount',
            ])

            ->add('discountType', ChoiceType::class, [
                'label' => 'invoice.discount_type.type',
                'choices' => [
                    'invoice.discount_type.percent' => 'percent',
                    'invoice.discount_type.value' => 'value',
                ],
            ])

            ->add('date', DateType::class, [
                'label' => 'invoice.date',
            ])

            ->add('number', StaticType::class, [
                'label' => 'invoice.number',
            ])

            ->add('items', TextareaType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Invoice::class,
        ]);
    }
}
