<?php

namespace Tigris\InvoiceBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Form\Type\WysiwygType;
use Tigris\InvoiceBundle\Entity\Model;

class ModelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'invoice_model.name',
            ])

            ->add('header', WysiwygType::class, [
                'label' => 'invoice_model.header',
            ])

            ->add('footer', WysiwygType::class, [
                'label' => 'invoice_model.footer',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Model::class,
        ]);
    }
}
