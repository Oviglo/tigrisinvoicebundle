<?php

namespace Tigris\InvoiceBundle\Invoice;

class Calculator
{
    public static function amountET(float $amountIT, float $taxValue): float
    {
        // PRIXTTC*100/(100+TVA)
        return self::round($amountIT * 100 / (100 + $taxValue));
    }

    public static function amountIT(float $amountET, float $taxValue): float
    {
        return round($amountET + ($amountET * ($taxValue / 100)), 2);
    }

    public static function totalIT(array $items): float
    {
        $total = 0.0;
        foreach ($items as $item) {
            $amount = $item['amountIT'] ?? static::amountIT($item['amountET'], $item['tax'] ?? 0);
            $total += ($amount * (int) $item['quantity']);
        }

        return $total;
    }

    public static function taxPrice(float $ETPrice, float $taxValue): float
    {
        return round(($ETPrice * $taxValue) / 100, 2);
    }

    public static function round(float $price): float
    {
        list($whole, $decimal) = sscanf(number_format($price, 3), '%d.%d');
        $thirdDecimal = $decimal ? (int) \substr($decimal, strlen($decimal) - 1) : 0;

        if ($thirdDecimal >= 5) {
            return round($price, 2, \PHP_ROUND_HALF_UP);
        }

        return round($price, 2, \PHP_ROUND_HALF_DOWN);
    }
}
