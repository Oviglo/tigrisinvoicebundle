<?php

namespace Tigris\InvoiceBundle\Invoice;

class InvoiceItem
{
    private string $description;

    private float $quantity;

    private float $amountET;

    private float $amountIT;

    private array $taxes = [];

    public function __construct(string $description, float $quantity, float $amountIT, array $taxes = [])
    {
        $this->setDescription($description);
        $this->setTaxes($taxes);
        $this->setQuantity($quantity);
        $this->setAmountIT($amountIT);
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getQuantity(): float
    {
        return $this->quantity;
    }

    public function setQuantity(float $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getAmountET(): float
    {
        return $this->amountET;
    }

    public function setAmountET(float $amountET): self
    {
        $this->amountET = $amountET;

        return $this;
    }

    public function getTaxes(): array
    {
        return $this->taxes;
    }

    public function setTaxes(array $taxes): self
    {
        $this->taxes = $taxes;

        return $this;
    }

    public function getAmountIT(): float
    {
        return $this->amountIT;
    }

    public function setAmountIT(float $amountIT): self
    {
        $this->amountIT = $amountIT;

        $this->amountET = $amountIT;
        foreach ($this->taxes as $value) {
            $this->amountET = Calculator::amountET($amountIT, $value);
        }

        return $this;
    }

    public function toArray(): array
    {
        return [
            'designation' => $this->getDescription(),
            'description' => $this->getDescription(),
            'quantity' => $this->getQuantity(),
            'amountET' => $this->getAmountET(),
            'amountIT' => $this->getAmountIT(),
            'taxes' => $this->getTaxes(),
        ];
    }
}
