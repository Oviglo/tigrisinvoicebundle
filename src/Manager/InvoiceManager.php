<?php

namespace Tigris\InvoiceBundle\Manager;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Manager\ConfigManager;
use Tigris\InvoiceBundle\Entity\Customer;
use Tigris\InvoiceBundle\Entity\Invoice;
use Tigris\InvoiceBundle\Repository\CustomerRepository;

class InvoiceManager
{
    public function __construct(
        private readonly ConfigManager $configManager,
        private readonly TokenStorageInterface $token,
        private readonly CustomerRepository $customerRepository
    ) {
    }

    public function create(User $user = null): Invoice
    {
        if (!$user instanceof User) {
            $user = $this->token->getToken()->getUser();
        }

        if (!$user instanceof User) {
            throw new AccessDeniedHttpException('Unable to create invoice');
        }

        // Load customer
        $customer = $this->customerRepository->findOneByUser($user);
        if (null == $customer) {
            $customer = new Customer();
        }

        $prefix = $this->configManager->getValue('TigrisInvoiceBundle.invoice_prefix', '#');
        $invoiceNum = $this->configManager->getValue('TigrisInvoiceBundle.next_invoice_number', 0);

        return (new Invoice())
            ->setNumber($invoiceNum)
            ->setNumberPrefix($prefix)
            ->setVendorAddress($this->configManager->getValue('TigrisInvoiceBundle.default_address', ''))
            ->setCustomer($customer)
        ;
    }
}
