<?php

namespace Tigris\InvoiceBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Traits\RepositoryTrait;
use Tigris\InvoiceBundle\Entity\Invoice;

class InvoiceRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Invoice::class);
    }

    public function findData(array $criteria): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('e');

        if ('' !== $criteria['status']) {
            $queryBuilder->andWhere('e.status = :status')
                ->setParameter(':status', $criteria['status'])
            ;
        }

        if ('' !== $criteria['payment_status']) {
            $queryBuilder->andWhere('e.paymentStatus = :payment_status')
                ->setParameter(':payment_status', $criteria['payment_status'])
            ;
        }

        if ('' !== $criteria['shipping_status']) {
            $queryBuilder->andWhere('e.shippingStatus = :shipping_status')
                ->setParameter(':shipping_status', $criteria['shipping_status'])
            ;
        }

        if (isset($criteria['user']) && '' !== $criteria['user']) {
            $queryBuilder->andWhere('e.author = :user')
                ->setParameter(':user', $criteria['user'])
            ;
        }

        $this->addBasicCriteria($queryBuilder, $criteria);

        return new Paginator($queryBuilder, true);
    }
}
