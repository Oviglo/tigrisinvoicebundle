<?php

namespace Tigris\InvoiceBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\InvoiceBundle\Entity\Invoice;

class InvoiceVoter extends Voter
{
    public const VIEW = 'view';

    public function __construct(private readonly Security $security)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        return $subject instanceof Invoice && self::VIEW === $attribute;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if ('view' === $attribute) {
            if ($this->security->isGranted('ROLE_ADMIN')) {
                return true;
            }

            return $user instanceof User && $subject->getUser()->getId() === $user->getId();
        }

        return false;
    }
}
