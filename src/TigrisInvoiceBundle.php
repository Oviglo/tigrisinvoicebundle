<?php

namespace Tigris\InvoiceBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class TigrisInvoiceBundle extends AbstractBundle
{
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        /*$parameters = $container->parameters()
            ->set('tigris_invoice', $config)
        ;

        foreach ($config as $key => $value) {
            $parameters->set("tigris_invoice.$key", $value);
        }*/

        $container->import(__DIR__.'/../config/services.yaml');
    }
}
