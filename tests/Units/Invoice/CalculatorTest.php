<?php

namespace Tigris\InvoiceBundle\Tests\Units\Invoice;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Tigris\InvoiceBundle\Invoice\Calculator;

#[CoversClass(Calculator::class)]
class CalculatorTest extends TestCase
{
    #[DataProvider('amountETProvider')]
    public function testAmountET(float $amountIT, float $taxValue, float $expected): void
    {
        $result = Calculator::amountET($amountIT, $taxValue);
        static::assertEquals($expected, $result);
    }

    public static function amountETProvider(): \Iterator
    {
        yield [210.5, 20, 175.42];
        yield [2330.54, 20, 1942.12];
        yield [456.99, 5.5, 433.17];
        yield [98.99, 5.5, 93.83];
        yield [9999, 2.1, 9793.34];
    }

    #[DataProvider('amountITProvider')]
    public function testAmountIT(float $amountET, float $taxValue, float $expected): void
    {
        $result = Calculator::amountIT($amountET, $taxValue);
        static::assertEquals($expected, $result);
    }

    public static function amountITProvider(): \Iterator
    {
        yield [210.5, 20, 252.60];
        yield [2330.54, 20, 2796.65];
        yield [456.99, 5.5, 482.12];
        yield [98.99, 5.5, 104.43];
        yield [9999, 2.1, 10208.98];
    }

    #[DataProvider('totalITProvider')]
    public function testTotalIT(array $items, float $expected): void
    {
        $result = Calculator::totalIT($items);
        static::assertEquals($expected, $result);
    }

    public static function totalITProvider(): \Iterator
    {
        yield [[
            ['amountIT' => 25.99, 'quantity' => 1],
            ['amountIT' => 55.49, 'quantity' => 2],
            ['amountIT' => 454, 'quantity' => 2],
        ], 1044.97];

        yield [[
            ['amountET' => 25.99, 'tax' => 5.5, 'quantity' => 1],
            ['amountET' => 55.49, 'tax' => 20, 'quantity' => 2],
            ['amountIT' => 454, 'quantity' => 2],
        ], 1068.6];
    }
}
